import Vue from 'vue'
import App from './App'

import './uni.promisify.adaptor'

import uiEcharts from '@/uni_modules/ui-echarts'

Vue.config.productionTip = false
App.mpType = 'app'

Vue.use(uiEcharts)

const app = new Vue({
  ...App
})
app.$mount()
