/**
 * 图表类型
 * */
export const chartTypes = [
	{ type: 'line', name: '折线图' },
	{ type: 'bar', name: '柱状图' },
	{ type: 'pie', name: '饼图' },
	{ type: 'scatter', name: '散点图' },
	// { type: 'map', name: '地理坐标' },
	{ type: 'candlestick', name: 'K线图' },
	{ type: 'radar', name: '雷达图' },
	{ type: 'boxplot', name: '盒须图' },
	{ type: 'heatmap', name: '热力图' },
	{ type: 'graph', name: '关系图' },
	// { type: 'lines', name: '路径图' },
	{ type: 'tree', name: '树图' },
	{ type: 'treemap', name: '矩形树图' },
	{ type: 'sunburst', name: '旭日图' },
	{ type: 'parallel', name: '平行坐标系' },
	{ type: 'sankey', name: '桑基图' },
	{ type: 'funnel', name: '漏斗图' },
	{ type: 'gauge', name: '仪表盘' },
	// { type: 'pictorialBar', name: '象形柱图' },
	{ type: 'themeRiver', name: '主题河流图' },
	{ type: 'calendar', name: '日历坐标系' },
	{ type: 'custom', name: '自定义系列' },
	{ type: 'dataset', name: '数据集' },
	{ type: 'dataZoom', name: '数据区域缩放' },
	// { type: 'graphic', name: '图形组件' }, 不支持
	{ type: 'rich', name: '富文本' },
	{ type: 'all', name: '全部' }
]
