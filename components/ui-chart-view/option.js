import * as echarts from '@/uni_modules/ui-echarts/static/echarts.min.js';
import dataFlare from '@/static/js/data-flare.js';
import dataTreemap from '@/static/js/data-treemap.js';

export const line = {
	simple: {
		grid: {
			right: 20
		},
		xAxis: {
			type: 'category',
			data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
		},
		yAxis: {
			type: 'value'
		},
		series: [{
			data: [150, 230, 224, 218, 135, 147, 260],
			type: 'line'
		}]
	}
}

export const bar = {
	simple: {
		grid: {
			right: 20
		},
		xAxis: {
			type: 'category',
			data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
		},
		yAxis: {
			type: 'value'
		},
		series: [{
			data: [120, 200, 150, 80, 70, 110, 130],
			type: 'bar'
		}]
	}
}

export const pie = {
	simple: {
		title: {
			text: 'Referer of a Website',
			subtext: 'Fake Data',
			left: 'center'
		},
		tooltip: {
			trigger: 'item'
		},
		legend: {
			orient: 'vertical',
			bottom: 'bottom',
			itemWidth: 8,
			itemHeight: 8,
			itemGap: 4
		},
		series: [{
			name: 'Access From',
			type: 'pie',
			radius: '50%',
			data: [{
					value: 1048,
					name: 'Search Engine'
				},
				{
					value: 735,
					name: 'Direct'
				},
				{
					value: 580,
					name: 'Email'
				},
				{
					value: 484,
					name: 'Union Ads'
				},
				{
					value: 300,
					name: 'Video Ads'
				}
			],
			emphasis: {
				itemStyle: {
					shadowBlur: 10,
					shadowOffsetX: 0,
					shadowColor: 'rgba(0, 0, 0, 0.5)'
				}
			}
		}]
	}
}

export const scatter = {
	simple: {
		grid: {
			right: 20
		},
		xAxis: {},
		yAxis: {},
		series: [{
			symbolSize: 20,
			data: [
				[10.0, 8.04],
				[8.07, 6.95],
				[13.0, 7.58],
				[9.05, 8.81],
				[11.0, 8.33],
				[14.0, 7.66],
				[13.4, 6.81],
				[10.0, 6.33],
				[14.0, 8.96],
				[12.5, 6.82],
				[9.15, 7.2],
				[11.5, 7.2],
				[3.03, 4.23],
				[12.2, 7.83],
				[2.02, 4.47],
				[1.05, 3.33],
				[4.05, 4.96],
				[6.03, 7.24],
				[12.0, 6.26],
				[12.0, 8.84],
				[7.08, 5.82],
				[5.02, 5.68]
			],
			type: 'scatter'
		}]
	}
}

export const candlestick = {
	simple: {
		grid: {
			right: 20
		},
		xAxis: {
			data: ['2017-10-24', '2017-10-25', '2017-10-26', '2017-10-27']
		},
		yAxis: {},
		series: [{
			type: 'candlestick',
			data: [
				[20, 34, 10, 38],
				[40, 35, 30, 50],
				[31, 38, 33, 44],
				[38, 15, 5, 42]
			]
		}]
	}
}

export const radar = {
	simple: {
		title: {
			// text: 'Basic Radar Chart'
		},
		legend: {
			data: ['Allocated Budget', 'Actual Spending'],
			orient: 'vertical',
			bottom: 'bottom',
			itemWidth: 8,
			itemHeight: 8,
			itemGap: 4
		},
		radar: {
			// shape: 'circle',
			indicator: [{
					name: 'Sales',
					max: 6500
				},
				{
					name: 'Administration',
					max: 16000
				},
				{
					name: 'Information Technology',
					max: 30000
				},
				{
					name: 'Customer Support',
					max: 38000
				},
				{
					name: 'Development',
					max: 52000
				},
				{
					name: 'Marketing',
					max: 25000
				}
			]
		},
		series: [{
			name: 'Budget vs spending',
			type: 'radar',
			data: [{
					value: [4200, 3000, 20000, 35000, 50000, 18000],
					name: 'Allocated Budget'
				},
				{
					value: [5000, 14000, 28000, 26000, 42000, 21000],
					name: 'Actual Spending'
				}
			]
		}]
	}
}

export const boxplot = {
	simple: {
		title: [{
				text: 'Michelson-Morley Experiment',
				left: 'center'
			},
			{
				text: 'upper: Q3 + 1.5 * IQR \nlower: Q1 - 1.5 * IQR',
				borderColor: '#999',
				borderWidth: 1,
				textStyle: {
					fontWeight: 'normal',
					fontSize: 14,
					lineHeight: 20
				},
				left: '10%',
				top: '90%'
			}
		],
		dataset: [{
				// prettier-ignore
				source: [
					[850, 740, 900, 1070, 930, 850, 950, 980, 980, 880, 1000, 980, 930, 650, 760, 810, 1000,
						1000, 960, 960
					],
					[960, 940, 960, 940, 880, 800, 850, 880, 900, 840, 830, 790, 810, 880, 880, 830, 800,
						790, 760, 800
					],
					[880, 880, 880, 860, 720, 720, 620, 860, 970, 950, 880, 910, 850, 870, 840, 840, 850,
						840, 840, 840
					],
					[890, 810, 810, 820, 800, 770, 760, 740, 750, 760, 910, 920, 890, 860, 880, 720, 840,
						850, 850, 780
					],
					[890, 840, 780, 810, 760, 810, 790, 810, 820, 850, 870, 870, 810, 740, 810, 940, 950,
						800, 810, 870
					]
				]
			},
			{
				transform: {
					type: 'boxplot',
					config: {
						itemNameFormatter: 'expr {value}'
					}
				}
			},
			{
				fromDatasetIndex: 1,
				fromTransformResult: 1
			}
		],
		tooltip: {
			trigger: 'item',
			axisPointer: {
				type: 'shadow'
			}
		},
		grid: {
			left: '10%',
			right: '10%',
			bottom: '15%'
		},
		xAxis: {
			type: 'category',
			boundaryGap: true,
			nameGap: 30,
			splitArea: {
				show: false
			},
			splitLine: {
				show: false
			}
		},
		yAxis: {
			type: 'value',
			name: 'km/s minus 299,000',
			splitArea: {
				show: true
			}
		},
		series: [{
				name: 'boxplot',
				type: 'boxplot',
				datasetIndex: 1
			},
			{
				name: 'outlier',
				type: 'scatter',
				datasetIndex: 2
			}
		]
	}
}

export const heatmap = {
	simple: {
		tooltip: {
			position: 'top'
		},
		grid: {
			height: '50%',
			top: '10%'
		},
		xAxis: {
			type: 'category',
			data: [
				'12a', '1a', '2a', '3a', '4a', '5a', '6a',
				'7a', '8a', '9a', '10a', '11a',
				'12p', '1p', '2p', '3p', '4p', '5p',
				'6p', '7p', '8p', '9p', '10p', '11p'
			],
			splitArea: {
				show: true
			}
		},
		yAxis: {
			type: 'category',
			data: [
				'Saturday', 'Friday', 'Thursday',
				'Wednesday', 'Tuesday', 'Monday', 'Sunday'
			],
			splitArea: {
				show: true
			}
		},
		visualMap: {
			min: 0,
			max: 10,
			calculable: true,
			orient: 'horizontal',
			left: 'center',
			bottom: '15%'
		},
		series: [{
			name: 'Punch Card',
			type: 'heatmap',
			data: [
					[0, 0, 5],
					[0, 1, 1],
					[0, 2, 0],
					[0, 3, 0],
					[0, 4, 0],
					[0, 5, 0],
					[0, 6, 0],
					[0, 7, 0],
					[0, 8, 0],
					[0, 9, 0],
					[0, 10, 0],
					[0, 11, 2],
					[0, 12, 4],
					[0, 13, 1],
					[0, 14, 1],
					[0, 15, 3],
					[0, 16, 4],
					[0, 17, 6],
					[0, 18, 4],
					[0, 19, 4],
					[0, 20, 3],
					[0, 21, 3],
					[0, 22, 2],
					[0, 23, 5],
					[1, 0, 7],
					[1, 1, 0],
					[1, 2, 0],
					[1, 3, 0],
					[1, 4, 0],
					[1, 5, 0],
					[1, 6, 0],
					[1, 7, 0],
					[1, 8, 0],
					[1, 9, 0],
					[1, 10, 5],
					[1, 11, 2],
					[1, 12, 2],
					[1, 13, 6],
					[1, 14, 9],
					[1, 15, 11],
					[1, 16, 6],
					[1, 17, 7],
					[1, 18, 8],
					[1, 19, 12],
					[1, 20, 5],
					[1, 21, 5],
					[1, 22, 7],
					[1, 23, 2],
					[2, 0, 1],
					[2, 1, 1],
					[2, 2, 0],
					[2, 3, 0],
					[2, 4, 0],
					[2, 5, 0],
					[2, 6, 0],
					[2, 7, 0],
					[2, 8, 0],
					[2, 9, 0],
					[2, 10, 3],
					[2, 11, 2],
					[2, 12, 1],
					[2, 13, 9],
					[2, 14, 8],
					[2, 15, 10],
					[2, 16, 6],
					[2, 17, 5],
					[2, 18, 5],
					[2, 19, 5],
					[2, 20, 7],
					[2, 21, 4],
					[2, 22, 2],
					[2, 23, 4],
					[3, 0, 7],
					[3, 1, 3],
					[3, 2, 0],
					[3, 3, 0],
					[3, 4, 0],
					[3, 5, 0],
					[3, 6, 0],
					[3, 7, 0],
					[3, 8, 1],
					[3, 9, 0],
					[3, 10, 5],
					[3, 11, 4],
					[3, 12, 7],
					[3, 13, 14],
					[3, 14, 13],
					[3, 15, 12],
					[3, 16, 9],
					[3, 17, 5],
					[3, 18, 5],
					[3, 19, 10],
					[3, 20, 6],
					[3, 21, 4],
					[3, 22, 4],
					[3, 23, 1],
					[4, 0, 1],
					[4, 1, 3],
					[4, 2, 0],
					[4, 3, 0],
					[4, 4, 0],
					[4, 5, 1],
					[4, 6, 0],
					[4, 7, 0],
					[4, 8, 0],
					[4, 9, 2],
					[4, 10, 4],
					[4, 11, 4],
					[4, 12, 2],
					[4, 13, 4],
					[4, 14, 4],
					[4, 15, 14],
					[4, 16, 12],
					[4, 17, 1],
					[4, 18, 8],
					[4, 19, 5],
					[4, 20, 3],
					[4, 21, 7],
					[4, 22, 3],
					[4, 23, 0],
					[5, 0, 2],
					[5, 1, 1],
					[5, 2, 0],
					[5, 3, 3],
					[5, 4, 0],
					[5, 5, 0],
					[5, 6, 0],
					[5, 7, 0],
					[5, 8, 2],
					[5, 9, 0],
					[5, 10, 4],
					[5, 11, 1],
					[5, 12, 5],
					[5, 13, 10],
					[5, 14, 5],
					[5, 15, 7],
					[5, 16, 11],
					[5, 17, 6],
					[5, 18, 0],
					[5, 19, 5],
					[5, 20, 3],
					[5, 21, 4],
					[5, 22, 2],
					[5, 23, 0],
					[6, 0, 1],
					[6, 1, 0],
					[6, 2, 0],
					[6, 3, 0],
					[6, 4, 0],
					[6, 5, 0],
					[6, 6, 0],
					[6, 7, 0],
					[6, 8, 0],
					[6, 9, 0],
					[6, 10, 1],
					[6, 11, 0],
					[6, 12, 2],
					[6, 13, 1],
					[6, 14, 3],
					[6, 15, 4],
					[6, 16, 0],
					[6, 17, 0],
					[6, 18, 0],
					[6, 19, 0],
					[6, 20, 1],
					[6, 21, 2],
					[6, 22, 2],
					[6, 23, 6]
				]
				.map(function(item) {
					return [item[1], item[0], item[2] || '-'];
				}),
			label: {
				show: true
			},
			emphasis: {
				itemStyle: {
					shadowBlur: 10,
					shadowColor: 'rgba(0, 0, 0, 0.5)'
				}
			}
		}]
	}
}

function createNodes(count) {
	var nodes = [];
	for (var i = 0; i < count; i++) {
		nodes.push({
			id: i + ''
		});
	}
	return nodes;
}

function createEdges(count) {
	var edges = [];
	if (count === 2) {
		return [
			[0, 1]
		];
	}
	for (var i = 0; i < count; i++) {
		edges.push([i, (i + 1) % count]);
	}
	return edges;
}
var graphSimpleDatas = [];
for (var i = 0; i < 16; i++) {
	graphSimpleDatas.push({
		nodes: createNodes(i + 2),
		edges: createEdges(i + 2)
	});
}
export const graph = {
	simple: {
		series: graphSimpleDatas.map(function(item, idx) {
			return {
				type: 'graph',
				layout: 'force',
				animation: false,
				data: item.nodes,
				left: (idx % 4) * 25 + '%',
				top: Math.floor(idx / 4) * 25 + '%',
				width: '25%',
				height: '25%',
				force: {
					// initLayout: 'circular'
					// gravity: 0
					repulsion: 60,
					edgeLength: 2
				},
				edges: item.edges.map(function(e) {
					return {
						source: e[0] + '',
						target: e[1] + ''
					};
				})
			};
		})
	}
}

dataFlare.children.forEach(function(datum, index) {
	index % 2 === 0 && (datum.collapsed = true);
});
export const tree = {
	simple: {
		tooltip: {
			trigger: 'item',
			triggerOn: 'mousemove'
		},
		series: [{
			type: 'tree',
			data: [dataFlare],
			top: '1%',
			left: '7%',
			bottom: '1%',
			right: '20%',
			symbolSize: 7,
			label: {
				position: 'left',
				verticalAlign: 'middle',
				align: 'right',
				fontSize: 9
			},
			leaves: {
				label: {
					position: 'right',
					verticalAlign: 'middle',
					align: 'left'
				}
			},
			emphasis: {
				focus: 'descendant'
			},
			expandAndCollapse: true,
			animationDuration: 550,
			animationDurationUpdate: 750
		}]
	}
}

export const treemap = {
	simple: {
		series: [{
			type: 'treemap',
			id: 'echarts-package-size',
			animationDurationUpdate: 1000,
			roam: false,
			nodeClick: undefined,
			data: dataTreemap.children,
			universalTransition: true,
			label: {
				show: true
			},
			breadcrumb: {
				show: false
			}
		}]
	}
}

export const sunburst = {
	simple: {
		series: {
			type: 'sunburst',
			// emphasis: {
			//     focus: 'ancestor'
			// },
			data: [{
					name: 'Grandpa',
					children: [{
							name: 'Uncle Leo',
							value: 15,
							children: [{
									name: 'Cousin Jack',
									value: 2
								},
								{
									name: 'Cousin Mary',
									value: 5,
									children: [{
										name: 'Jackson',
										value: 2
									}]
								},
								{
									name: 'Cousin Ben',
									value: 4
								}
							]
						},
						{
							name: 'Father',
							value: 10,
							children: [{
									name: 'Me',
									value: 5
								},
								{
									name: 'Brother Peter',
									value: 1
								}
							]
						}
					]
				},
				{
					name: 'Nancy',
					children: [{
						name: 'Uncle Nike',
						children: [{
								name: 'Cousin Betty',
								value: 1
							},
							{
								name: 'Cousin Jenny',
								value: 2
							}
						]
					}]
				}
			],
			radius: [0, '90%'],
			label: {
				rotate: 'radial'
			}
		}
	}
}

export const parallel = {
	simple: {
		parallelAxis: [{
				dim: 0,
				name: 'Price'
			},
			{
				dim: 1,
				name: 'Net Weight'
			},
			{
				dim: 2,
				name: 'Amount'
			},
			{
				dim: 3,
				name: 'Score',
				type: 'category',
				data: ['Excellent', 'Good', 'OK', 'Bad']
			}
		],
		series: {
			type: 'parallel',
			lineStyle: {
				width: 4
			},
			data: [
				[12.99, 100, 82, 'Good'],
				[9.99, 80, 77, 'OK'],
				[20, 120, 60, 'Excellent']
			]
		}
	}
}

export const sankey = {
	simple: {
		series: {
			type: 'sankey',
			layout: 'none',
			emphasis: {
				focus: 'adjacency'
			},
			data: [{
					name: 'a'
				},
				{
					name: 'b'
				},
				{
					name: 'a1'
				},
				{
					name: 'a2'
				},
				{
					name: 'b1'
				},
				{
					name: 'c'
				}
			],
			links: [{
					source: 'a',
					target: 'a1',
					value: 5
				},
				{
					source: 'a',
					target: 'a2',
					value: 3
				},
				{
					source: 'b',
					target: 'b1',
					value: 8
				},
				{
					source: 'a',
					target: 'b1',
					value: 3
				},
				{
					source: 'b1',
					target: 'a1',
					value: 1
				},
				{
					source: 'b1',
					target: 'c',
					value: 2
				}
			]
		}
	}
}

export const funnel = {
	simple: {
		title: {
			text: 'Funnel'
		},
		tooltip: {
			trigger: 'item',
			formatter: '{a} <br/>{b} : {c}%'
		},
		/* toolbox: {
			feature: {
				dataView: {
					readOnly: false
				},
				restore: {},
				saveAsImage: {}
			}
		}, */
		legend: {
			data: ['Show', 'Click', 'Visit', 'Inquiry', 'Order'],
			orient: 'vertical',
			bottom: 'bottom',
			itemWidth: 8,
			itemHeight: 8,
			itemGap: 4
		},
		series: [{
			name: 'Funnel',
			type: 'funnel',
			left: '10%',
			top: 60,
			bottom: 60,
			width: '80%',
			min: 0,
			max: 100,
			minSize: '0%',
			maxSize: '100%',
			sort: 'descending',
			gap: 2,
			label: {
				show: true,
				position: 'inside'
			},
			labelLine: {
				length: 10,
				lineStyle: {
					width: 1,
					type: 'solid'
				}
			},
			itemStyle: {
				borderColor: '#fff',
				borderWidth: 1
			},
			emphasis: {
				label: {
					fontSize: 20
				}
			},
			data: [{
					value: 60,
					name: 'Visit'
				},
				{
					value: 40,
					name: 'Inquiry'
				},
				{
					value: 20,
					name: 'Order'
				},
				{
					value: 80,
					name: 'Click'
				},
				{
					value: 100,
					name: 'Show'
				}
			]
		}]
	}
}

export const gauge = {
	simple: {
		tooltip: {
			formatter: '{a} <br/>{b} : {c}%'
		},
		series: [{
			name: 'Pressure',
			type: 'gauge',
			detail: {
				formatter: '{value}'
			},
			data: [{
				value: 50,
				name: 'SCORE'
			}]
		}]
	}
}

export const themeRiver = {
	simple: {
		tooltip: {
			trigger: 'axis',
			axisPointer: {
				type: 'line',
				lineStyle: {
					color: 'rgba(0,0,0,0.2)',
					width: 1,
					type: 'solid'
				}
			}
		},
		legend: {
			data: ['DQ', 'TY', 'SS', 'QG', 'SY', 'DD']
		},
		singleAxis: {
			top: 50,
			bottom: 50,
			axisTick: {},
			axisLabel: {},
			type: 'time',
			axisPointer: {
				animation: true,
				label: {
					show: true
				}
			},
			splitLine: {
				show: true,
				lineStyle: {
					type: 'dashed',
					opacity: 0.2
				}
			}
		},
		series: [{
			type: 'themeRiver',
			emphasis: {
				itemStyle: {
					shadowBlur: 20,
					shadowColor: 'rgba(0, 0, 0, 0.8)'
				}
			},
			data: [
				['2015/11/08', 10, 'DQ'],
				['2015/11/09', 15, 'DQ'],
				['2015/11/10', 35, 'DQ'],
				['2015/11/11', 38, 'DQ'],
				['2015/11/12', 22, 'DQ'],
				['2015/11/13', 16, 'DQ'],
				['2015/11/14', 7, 'DQ'],
				['2015/11/15', 2, 'DQ'],
				['2015/11/16', 17, 'DQ'],
				['2015/11/17', 33, 'DQ'],
				['2015/11/18', 40, 'DQ'],
				['2015/11/19', 32, 'DQ'],
				['2015/11/20', 26, 'DQ'],
				['2015/11/21', 35, 'DQ'],
				['2015/11/22', 40, 'DQ'],
				['2015/11/23', 32, 'DQ'],
				['2015/11/24', 26, 'DQ'],
				['2015/11/25', 22, 'DQ'],
				['2015/11/26', 16, 'DQ'],
				['2015/11/27', 22, 'DQ'],
				['2015/11/28', 10, 'DQ'],
				['2015/11/08', 35, 'TY'],
				['2015/11/09', 36, 'TY'],
				['2015/11/10', 37, 'TY'],
				['2015/11/11', 22, 'TY'],
				['2015/11/12', 24, 'TY'],
				['2015/11/13', 26, 'TY'],
				['2015/11/14', 34, 'TY'],
				['2015/11/15', 21, 'TY'],
				['2015/11/16', 18, 'TY'],
				['2015/11/17', 45, 'TY'],
				['2015/11/18', 32, 'TY'],
				['2015/11/19', 35, 'TY'],
				['2015/11/20', 30, 'TY'],
				['2015/11/21', 28, 'TY'],
				['2015/11/22', 27, 'TY'],
				['2015/11/23', 26, 'TY'],
				['2015/11/24', 15, 'TY'],
				['2015/11/25', 30, 'TY'],
				['2015/11/26', 35, 'TY'],
				['2015/11/27', 42, 'TY'],
				['2015/11/28', 42, 'TY'],
				['2015/11/08', 21, 'SS'],
				['2015/11/09', 25, 'SS'],
				['2015/11/10', 27, 'SS'],
				['2015/11/11', 23, 'SS'],
				['2015/11/12', 24, 'SS'],
				['2015/11/13', 21, 'SS'],
				['2015/11/14', 35, 'SS'],
				['2015/11/15', 39, 'SS'],
				['2015/11/16', 40, 'SS'],
				['2015/11/17', 36, 'SS'],
				['2015/11/18', 33, 'SS'],
				['2015/11/19', 43, 'SS'],
				['2015/11/20', 40, 'SS'],
				['2015/11/21', 34, 'SS'],
				['2015/11/22', 28, 'SS'],
				['2015/11/23', 26, 'SS'],
				['2015/11/24', 37, 'SS'],
				['2015/11/25', 41, 'SS'],
				['2015/11/26', 46, 'SS'],
				['2015/11/27', 47, 'SS'],
				['2015/11/28', 41, 'SS'],
				['2015/11/08', 10, 'QG'],
				['2015/11/09', 15, 'QG'],
				['2015/11/10', 35, 'QG'],
				['2015/11/11', 38, 'QG'],
				['2015/11/12', 22, 'QG'],
				['2015/11/13', 16, 'QG'],
				['2015/11/14', 7, 'QG'],
				['2015/11/15', 2, 'QG'],
				['2015/11/16', 17, 'QG'],
				['2015/11/17', 33, 'QG'],
				['2015/11/18', 40, 'QG'],
				['2015/11/19', 32, 'QG'],
				['2015/11/20', 26, 'QG'],
				['2015/11/21', 35, 'QG'],
				['2015/11/22', 40, 'QG'],
				['2015/11/23', 32, 'QG'],
				['2015/11/24', 26, 'QG'],
				['2015/11/25', 22, 'QG'],
				['2015/11/26', 16, 'QG'],
				['2015/11/27', 22, 'QG'],
				['2015/11/28', 10, 'QG'],
				['2015/11/08', 10, 'SY'],
				['2015/11/09', 15, 'SY'],
				['2015/11/10', 35, 'SY'],
				['2015/11/11', 38, 'SY'],
				['2015/11/12', 22, 'SY'],
				['2015/11/13', 16, 'SY'],
				['2015/11/14', 7, 'SY'],
				['2015/11/15', 2, 'SY'],
				['2015/11/16', 17, 'SY'],
				['2015/11/17', 33, 'SY'],
				['2015/11/18', 40, 'SY'],
				['2015/11/19', 32, 'SY'],
				['2015/11/20', 26, 'SY'],
				['2015/11/21', 35, 'SY'],
				['2015/11/22', 4, 'SY'],
				['2015/11/23', 32, 'SY'],
				['2015/11/24', 26, 'SY'],
				['2015/11/25', 22, 'SY'],
				['2015/11/26', 16, 'SY'],
				['2015/11/27', 22, 'SY'],
				['2015/11/28', 10, 'SY'],
				['2015/11/08', 10, 'DD'],
				['2015/11/09', 15, 'DD'],
				['2015/11/10', 35, 'DD'],
				['2015/11/11', 38, 'DD'],
				['2015/11/12', 22, 'DD'],
				['2015/11/13', 16, 'DD'],
				['2015/11/14', 7, 'DD'],
				['2015/11/15', 2, 'DD'],
				['2015/11/16', 17, 'DD'],
				['2015/11/17', 33, 'DD'],
				['2015/11/18', 4, 'DD'],
				['2015/11/19', 32, 'DD'],
				['2015/11/20', 26, 'DD'],
				['2015/11/21', 35, 'DD'],
				['2015/11/22', 40, 'DD'],
				['2015/11/23', 32, 'DD'],
				['2015/11/24', 26, 'DD'],
				['2015/11/25', 22, 'DD'],
				['2015/11/26', 16, 'DD'],
				['2015/11/27', 22, 'DD'],
				['2015/11/28', 10, 'DD']
			]
		}]
	}
}

function getVirtualData(year) {
	const date = +echarts.time.parse(year + '-01-01');
	const end = +echarts.time.parse(year + '-12-31');
	const dayTime = 3600 * 24 * 1000;
	const data = [];
	for (let time = date; time <= end; time += dayTime) {
		data.push([
			echarts.time.format(time, '{yyyy}-{MM}-{dd}', false),
			Math.floor(Math.random() * 10000)
		]);
	}
	return data;
}
export const calendar = {
	simple: {
		visualMap: {
			show: false,
			min: 0,
			max: 10000
		},
		calendar: {
			range: '2023'
		},
		series: {
			type: 'heatmap',
			coordinateSystem: 'calendar',
			data: getVirtualData('2023')
		}
	}
}

const colorList = ['#4f81bd','#c0504d','#9bbb59','#604a7b','#948a54','#e46c0b'];
const customData = [
  [10, 16, 3, 'A'],
  [16, 18, 15, 'B'],
  [18, 26, 12, 'C'],
  [26, 32, 22, 'D'],
  [32, 56, 7, 'E'],
  [56, 62, 17, 'F']
].map(function (item, index) {
  return {
    value: item,
    itemStyle: {
      color: colorList[index]
    }
  };
});
export const custom = {
	simple: {
		title: {
			text: 'Profit',
			left: 'center'
		},
		tooltip: {},
		xAxis: {
			scale: true
		},
		yAxis: {},
		series: [{
			type: 'custom',
			renderItem: function(params, api) {
				var yValue = api.value(2);
				var start = api.coord([api.value(0), yValue]);
				var size = api.size([api.value(1) - api.value(0), yValue]);
				var style = api.style();
				return {
					type: 'rect',
					shape: {
						x: start[0],
						y: start[1],
						width: size[0],
						height: size[1]
					},
					style: style
				};
			},
			label: {
				show: true,
				position: 'top'
			},
			dimensions: ['from', 'to', 'profit'],
			encode: {
				x: [0, 1],
				y: 2,
				tooltip: [0, 1, 2],
				itemName: 3
			},
			data: customData
		}]
	}
}

export const dataset = {
	simple: {
		grid: {
			right: '5%'
		},
		dataset: [{
				dimensions: ['name', 'age', 'profession', 'score', 'date'],
				source: [
					['Hannah Krause', 41, 'Engineer', 314, '2011-02-12'],
					['Zhao Qian', 20, 'Teacher', 351, '2011-03-01'],
					['Jasmin Krause ', 52, 'Musician', 287, '2011-02-14'],
					['Li Lei', 37, 'Teacher', 219, '2011-02-18'],
					['Karle Neumann', 25, 'Engineer', 253, '2011-04-02'],
					['Adrian Groß', 19, 'Teacher', '-', '2011-01-16'],
					['Mia Neumann', 71, 'Engineer', 165, '2011-03-19'],
					['Böhm Fuchs', 36, 'Musician', 318, '2011-02-24'],
					['Han Meimei', 67, 'Engineer', 366, '2011-03-12']
				]
			},
			{
				transform: {
					type: 'sort',
					config: {
						dimension: 'score',
						order: 'desc'
					}
				}
			}
		],
		xAxis: {
			type: 'category',
			axisLabel: {
				interval: 0,
				rotate: 30
			}
		},
		yAxis: {},
		series: {
			type: 'bar',
			encode: {
				x: 'name',
				y: 'score'
			},
			datasetIndex: 1
		}
	}
}

const dimensions = [
	'name', 'Price', 'Prime cost', 'Prime cost min', 'Prime cost max', 'Price min', 'Price max'
];
// prettier-ignore
const dataZoomData = [
	['Blouse "Blue Viola"', 101.88, 99.75, 76.75, 116.75, 69.88, 119.88],
	['Dress "Daisy"', 155.8, 144.03, 126.03, 156.03, 129.8, 188.8],
	['Trousers "Cutesy Classic"', 203.25, 173.56, 151.56, 187.56, 183.25, 249.25],
	['Dress "Morning Dew"', 256, 120.5, 98.5, 136.5, 236, 279],
	['Turtleneck "Dark Chocolate"', 408.89, 294.75, 276.75, 316.75, 385.89, 427.89],
	['Jumper "Early Spring"', 427.36, 430.24, 407.24, 452.24, 399.36, 461.36],
	['Breeches "Summer Mood"', 356, 135.5, 123.5, 151.5, 333, 387],
	['Dress "Mauve Chamomile"', 406, 95.5, 73.5, 111.5, 366, 429],
	['Dress "Flying Tits"', 527.36, 503.24, 488.24, 525.24, 485.36, 551.36],
	['Dress "Singing Nightingales"', 587.36, 543.24, 518.24, 555.24, 559.36, 624.36],
	['Sundress "Cloudy weather"', 603.36, 407.24, 392.24, 419.24, 581.36, 627.36],
	['Sundress "East motives"', 633.36, 477.24, 445.24, 487.24, 594.36, 652.36],
	['Sweater "Cold morning"', 517.36, 437.24, 416.24, 454.24, 488.36, 565.36],
	['Trousers "Lavender Fields"', 443.36, 387.24, 370.24, 413.24, 412.36, 484.36],
	['Jumper "Coffee with Milk"', 543.36, 307.24, 288.24, 317.24, 509.36, 574.36],
	['Blouse "Blooming Cactus"', 790.36, 277.24, 254.24, 295.24, 764.36, 818.36],
	['Sweater "Fluffy Comfort"', 790.34, 678.34, 660.34, 690.34, 762.34, 824.34]
];

function renderItem(params, api) {
	const group = {
		type: 'group',
		children: []
	};
	let coordDims = ['x', 'y'];
	for (let baseDimIdx = 0; baseDimIdx < 2; baseDimIdx++) {
		let otherDimIdx = 1 - baseDimIdx;
		let encode = params.encode;
		let baseValue = api.value(encode[coordDims[baseDimIdx]][0]);
		let param = [];
		param[baseDimIdx] = baseValue;
		param[otherDimIdx] = api.value(encode[coordDims[otherDimIdx]][1]);
		let highPoint = api.coord(param);
		param[otherDimIdx] = api.value(encode[coordDims[otherDimIdx]][2]);
		let lowPoint = api.coord(param);
		let halfWidth = 5;
		var style = api.style({
			stroke: api.visual('color'),
			fill: undefined
		});
		group.children.push({
			type: 'line',
			transition: ['shape'],
			shape: makeShape(
				baseDimIdx,
				highPoint[baseDimIdx] - halfWidth,
				highPoint[otherDimIdx],
				highPoint[baseDimIdx] + halfWidth,
				highPoint[otherDimIdx]
			),
			style: style
		}, {
			type: 'line',
			transition: ['shape'],
			shape: makeShape(
				baseDimIdx,
				highPoint[baseDimIdx],
				highPoint[otherDimIdx],
				lowPoint[baseDimIdx],
				lowPoint[otherDimIdx]
			),
			style: style
		}, {
			type: 'line',
			transition: ['shape'],
			shape: makeShape(
				baseDimIdx,
				lowPoint[baseDimIdx] - halfWidth,
				lowPoint[otherDimIdx],
				lowPoint[baseDimIdx] + halfWidth,
				lowPoint[otherDimIdx]
			),
			style: style
		});
	}

	function makeShape(baseDimIdx, base1, value1, base2, value2) {
		var shape = {};
		shape[coordDims[baseDimIdx] + '1'] = base1;
		shape[coordDims[1 - baseDimIdx] + '1'] = value1;
		shape[coordDims[baseDimIdx] + '2'] = base2;
		shape[coordDims[1 - baseDimIdx] + '2'] = value2;
		return shape;
	}
	return group;
}
export const dataZoom = {
	simple: {
		tooltip: {},
		legend: {
			data: ['bar', 'error']
		},
		dataZoom: [{
				type: 'slider'
			},
			{
				type: 'inside'
			}
		],
		grid: {
			bottom: 80
		},
		xAxis: {},
		yAxis: {},
		series: [{
				type: 'scatter',
				name: 'error',
				data: dataZoomData,
				dimensions: dimensions,
				encode: {
					x: 2,
					y: 1,
					tooltip: [2, 1, 3, 4, 5, 6],
					itemName: 0
				},
				itemStyle: {
					color: '#77bef7'
				}
			},
			{
				type: 'custom',
				name: 'error',
				renderItem: renderItem,
				dimensions: dimensions,
				encode: {
					x: [2, 3, 4],
					y: [1, 5, 6],
					tooltip: [2, 1, 3, 4, 5, 6],
					itemName: 0
				},
				data: dataZoomData,
				z: 100
			}
		]
	}
}

export const rich = {
	simple: {
		tooltip: {
			trigger: 'item',
			formatter: '{a} <br/>{b}: {c} ({d}%)'
		},
		legend: {
			data: [
				'Direct',
				'Marketing',
				'Search Engine',
				'Email',
				'Union Ads',
				'Video Ads',
				'Baidu',
				'Google',
				'Bing',
				'Others'
			],
			orient: 'vertical',
			bottom: 'bottom',
			itemWidth: 8,
			itemHeight: 8,
			itemGap: 4
		},
		series: [{
				name: 'Access From',
				type: 'pie',
				selectedMode: 'single',
				radius: [0, '30%'],
				label: {
					position: 'inner',
					fontSize: 14
				},
				labelLine: {
					show: false
				},
				data: [{
						value: 1548,
						name: 'Search Engine'
					},
					{
						value: 775,
						name: 'Direct'
					},
					{
						value: 679,
						name: 'Marketing',
						selected: true
					}
				]
			},
			{
				name: 'Access From',
				type: 'pie',
				radius: ['45%', '60%'],
				labelLine: {
					length: 30
				},
				label: {
					formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ',
					backgroundColor: '#F6F8FC',
					borderColor: '#8C8D8E',
					borderWidth: 1,
					borderRadius: 4,
					rich: {
						a: {
							color: '#6E7079',
							lineHeight: 22,
							align: 'center'
						},
						hr: {
							borderColor: '#8C8D8E',
							width: '100%',
							borderWidth: 1,
							height: 0
						},
						b: {
							color: '#4C5058',
							fontSize: 14,
							fontWeight: 'bold',
							lineHeight: 33
						},
						per: {
							color: '#fff',
							backgroundColor: '#4C5058',
							padding: [3, 4],
							borderRadius: 4
						}
					}
				},
				data: [{
						value: 1048,
						name: 'Baidu'
					},
					{
						value: 335,
						name: 'Direct'
					},
					{
						value: 310,
						name: 'Email'
					},
					{
						value: 251,
						name: 'Google'
					},
					{
						value: 234,
						name: 'Union Ads'
					},
					{
						value: 147,
						name: 'Bing'
					},
					{
						value: 135,
						name: 'Video Ads'
					},
					{
						value: 102,
						name: 'Others'
					}
				]
			}
		]
	}
}